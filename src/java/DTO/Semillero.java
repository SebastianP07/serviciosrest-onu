package DTO;

public class Semillero {
    private String id;
    private String idGrupoInvestigacion;
    private String idCiudad; 
    private String direccion;
    private String paginaWeb; 
    private String email;
    private String nombre;
    private String idAreaConocimiento; 
    private String mesConformacion;
    private String añoConformacion; 

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdAreaConocimiento() {
        return idAreaConocimiento;
    }

    public void setIdAreaConocimiento(String idAreaConocimiento) {
        this.idAreaConocimiento = idAreaConocimiento;
    }

    public String getMesConformacion() {
        return mesConformacion;
    }

    public void setMesConformacion(String mesConformacion) {
        this.mesConformacion = mesConformacion;
    }

    public String getAñoConformacion() {
        return añoConformacion;
    }

    public void setAñoConformacion(String añoConformacion) {
        this.añoConformacion = añoConformacion;
    }
    
}
