package DTO;

public class CursoProgramaAcademico {
    private String idCursoProgramaAcademico;
    private String idProgramaAcademico;
    private String idAreaConocimiento; 
    private String nombre;
    private String denominacion;
    private ProgramaAcademico programaAcademico; 
    private AreaConocimiento areaConocimiento;
    private TipoPrograma tipoPrograma;
    private Institucion institucion;

    public String getIdCursoProgramaAcademico() {
        return idCursoProgramaAcademico;
    }

    public void setIdCursoProgramaAcademico(String idCursoProgramaAcademico) {
        this.idCursoProgramaAcademico = idCursoProgramaAcademico;
    }

    public String getIdProgramaAcademico() {
        return idProgramaAcademico;
    }

    public void setIdProgramaAcademico(String idProgramaAcademico) {
        this.idProgramaAcademico = idProgramaAcademico;
    }

    public String getIdAreaConocimiento() {
        return idAreaConocimiento;
    }

    public void setIdAreaConocimiento(String idAreaConocimiento) {
        this.idAreaConocimiento = idAreaConocimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public ProgramaAcademico getProgramaAcademico() {
        return programaAcademico;
    }

    public void setProgramaAcademico(ProgramaAcademico programaAcademico) {
        this.programaAcademico = programaAcademico;
    }

    public AreaConocimiento getAreaConocimiento() {
        return areaConocimiento;
    }

    public void setAreaConocimiento(AreaConocimiento areaConocimiento) {
        this.areaConocimiento = areaConocimiento;
    }

    public TipoPrograma getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(TipoPrograma tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }
   
}