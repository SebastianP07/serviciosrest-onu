package DTO;

public class Conferencia {
    private String id;
    private String idMiembro;
    private String fecha;
    private String idCiudad;
    private String copiaCertificado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getCopiaCertificado() {
        return copiaCertificado;
    }

    public void setCopiaCertificado(String copiaCertificado) {
        this.copiaCertificado = copiaCertificado;
    }
}
