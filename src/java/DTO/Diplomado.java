package DTO;

public class Diplomado {
    private String idDiplomado;
    private String idGrupoInvestigacion;
    private String idAreaConocimiento; 
    private String descripcion;
    private String fechaPrimerCorte;
    private String idInstitucion; 
    private String nombre;
    private String denominacion;
    private AreaConocimiento areaConocimiento;
    private Institucion institucion;

    public String getIdDiplomado() {
        return idDiplomado;
    }

    public void setIdDiplomado(String idDiplomado) {
        this.idDiplomado = idDiplomado;
    }

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getIdAreaConocimiento() {
        return idAreaConocimiento;
    }

    public void setIdAreaConocimiento(String idAreaConocimiento) {
        this.idAreaConocimiento = idAreaConocimiento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaPrimerCorte() {
        return fechaPrimerCorte;
    }

    public void setFechaPrimerCorte(String fechaPrimerCorte) {
        this.fechaPrimerCorte = fechaPrimerCorte;
    }

    public String getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(String idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public AreaConocimiento getAreaConocimiento() {
        return areaConocimiento;
    }

    public void setAreaConocimiento(AreaConocimiento areaConocimiento) {
        this.areaConocimiento = areaConocimiento;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }
    
    
}
