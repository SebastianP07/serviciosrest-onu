package DTO;

public class TipoPrograma {
    private String idTipoPrograma;
    private String tipoProgramaNombre;

    public String getIdTipoPrograma() {
        return idTipoPrograma;
    }

    public void setIdTipoPrograma(String idTipoPrograma) {
        this.idTipoPrograma = idTipoPrograma;
    }

    public String getTipoProgramaNombre() {
        return tipoProgramaNombre;
    }

    public void setTipoProgramaNombre(String tipoProgramaNombre) {
        this.tipoProgramaNombre = tipoProgramaNombre;
    }

    
}
