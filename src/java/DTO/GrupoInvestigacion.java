package DTO;

public class GrupoInvestigacion {

    private String idGrupoInvestigacion;
    private String paginaWeb;
    private String email;
    private String idAreaConocimiento;
    private String idCiudad;
    private String mesConformacion;
    private String anioConformacion;
    private Instituciones instituciones;
    private MiembrosGrupo miembrosGrupoLider;
    private MiembrosGrupo miembrosGrupo;
    private AreaConocimiento areaConocimiento;
    private Ciudad ciudad;

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getPaginaWeb() {
        return paginaWeb;
    }

    public void setPaginaWeb(String paginaWeb) {
        this.paginaWeb = paginaWeb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdAreaConocimiento() {
        return idAreaConocimiento;
    }

    public void setIdAreaConocimiento(String idAreaConocimiento) {
        this.idAreaConocimiento = idAreaConocimiento;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getMesConformacion() {
        return mesConformacion;
    }

    public void setMesConformacion(String mesConformacion) {
        this.mesConformacion = mesConformacion;
    }

    public String getAnioConformacion() {
        return anioConformacion;
    }

    public void setAnioConformacion(String anioConformacion) {
        this.anioConformacion = anioConformacion;
    }

    public Instituciones getInstituciones() {
        return instituciones;
    }

    public void setInstituciones(Instituciones instituciones) {
        this.instituciones = instituciones;
    }

    public MiembrosGrupo getMiembrosGrupoLider() {
        return miembrosGrupoLider;
    }

    public void setMiembrosGrupoLider(MiembrosGrupo miembrosGrupoLider) {
        this.miembrosGrupoLider = miembrosGrupoLider;
    }

    public MiembrosGrupo getMiembrosGrupo() {
        return miembrosGrupo;
    }

    public void setMiembrosGrupo(MiembrosGrupo miembrosGrupo) {
        this.miembrosGrupo = miembrosGrupo;
    }

    public AreaConocimiento getAreaConocimiento() {
        return areaConocimiento;
    }

    public void setAreaConocimiento(AreaConocimiento areaConocimiento) {
        this.areaConocimiento = areaConocimiento;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    
}