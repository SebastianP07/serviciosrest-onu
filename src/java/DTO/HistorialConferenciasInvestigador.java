/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author JUAN
 */
public class HistorialConferenciasInvestigador {

    private String idHistorialConferenciasInvestigador;
    private String idMiembro;
    private String fecha;
    private String idCiudad;
    private String copiaCertificado;
    private Ciudad ciudad;
    private Departamento departamento;
    private Pais pais;

    public String getIdHistorialConferenciasInvestigador() {
        return idHistorialConferenciasInvestigador;
    }

    public void setIdHistorialConferenciasInvestigador(String idHistorialConferenciasInvestigador) {
        this.idHistorialConferenciasInvestigador = idHistorialConferenciasInvestigador;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getCopiaCertificado() {
        return copiaCertificado;
    }

    public void setCopiaCertificado(String copiaCertificado) {
        this.copiaCertificado = copiaCertificado;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
    
}
