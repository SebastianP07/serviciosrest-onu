/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

/**
 *
 * @author danie
 */
public class MiembrosSemillero {
    private String id;
    private String idSemillero;
    private String idMiembroGrupo;
    private String idRol;
    private String fechaVinculacion;
    private String idProgramaAcademico;
    private String fechaVinculacionInstitucion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSemillero() {
        return idSemillero;
    }

    public void setIdSemillero(String idSemillero) {
        this.idSemillero = idSemillero;
    }

    public String getIdMiembroGrupo() {
        return idMiembroGrupo;
    }

    public void setIdMiembroGrupo(String idMiembroGrupo) {
        this.idMiembroGrupo = idMiembroGrupo;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(String fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }

    public String getIdProgramaAcademico() {
        return idProgramaAcademico;
    }

    public void setIdProgramaAcademico(String idProgramaAcademico) {
        this.idProgramaAcademico = idProgramaAcademico;
    }

    public String getFechaVinculacionInstitucion() {
        return fechaVinculacionInstitucion;
    }

    public void setFechaVinculacionInstitucion(String fechaVinculacionInstitucion) {
        this.fechaVinculacionInstitucion = fechaVinculacionInstitucion;
    }
}