package DTO;

import java.sql.Date;

public class MiembrosGrupo {
    private String idMiembrosGrupo;
    private String idInvestigador;
    private String idRol;
    private String idGrupoInvestigacion;
    private String fechaVinculacion;

    public String getIdMiembrosGrupo() {
        return idMiembrosGrupo;
    }

    public void setIdMiembrosGrupo(String idMiembrosGrupo) {
        this.idMiembrosGrupo = idMiembrosGrupo;
    }

    public String getIdInvestigador() {
        return idInvestigador;
    }

    public void setIdInvestigador(String idInvestigador) {
        this.idInvestigador = idInvestigador;
    }

    public String getIdRol() {
        return idRol;
    }

    public void setIdRol(String idRol) {
        this.idRol = idRol;
    }

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(String fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }
    
}
