package DTO;

import java.util.ArrayList;

public class Respuesta {

    private int codigo;
    private String descripcion;
    private String descripcionError;
    private ArrayList<Integer> idMiembosGrupo;
    private ArrayList<Ciudad> objetoRespuestaArrayCiudades;
    private ArrayList<AreaConocimiento> objetoRespuestaArrayAreaConocimiento;
    private ArrayList<LineaInvestigacion> objetoRespuestaArrayLineaInvestigacion;
    private ArrayList<Institucion> objetoRespuestaArrayInstituciones;
    private ArrayList<GrupoInvestigacion> objetoRespuestaArrayGrupoInvestigacion;
    private ArrayList<TipoPrograma> objetoRespuestaArrayTipoPrograma;
    private Investigador objetoRespuestaInvestigador;
    private Object objetoRespuesta;
    private ArrayList<CursoProgramaAcademico> objetoRespuestaArrayCursoProgramaAcademico;
    private ArrayList<ProgramaAcademico> objetoRespuestaArrayProgramaAcademico;
    private ArrayList<ProgramaAcademico> objetoRespuestaArrayProgramaAcademicoAlmacenado;
    private ArrayList<Diplomado> objetoRespuestaArrayDiplomadoAlmacenado;
    private ArrayList<CursoProgramaAcademico> objetoRespuestaArrayCursoProgramaAcademicoAlmacenado;
    private ArrayList<HistorialInvestigadorGrupo> objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado;
    private ArrayList<HistorialConferenciasInvestigador> objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado;
    private ArrayList<MiembrosGrupo> objetoRespuestaArrayMiembrosGrupo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    public ArrayList<Integer> getIdMiembosGrupo() {
        return idMiembosGrupo;
    }

    public void setIdMiembosGrupo(ArrayList<Integer> idMiembosGrupo) {
        this.idMiembosGrupo = idMiembosGrupo;
    }

    public ArrayList<Ciudad> getObjetoRespuestaArrayCiudades() {
        return objetoRespuestaArrayCiudades;
    }

    public void setObjetoRespuestaArrayCiudades(ArrayList<Ciudad> objetoRespuestaArrayCiudades) {
        this.objetoRespuestaArrayCiudades = objetoRespuestaArrayCiudades;
    }

    public ArrayList<AreaConocimiento> getObjetoRespuestaArrayAreaConocimiento() {
        return objetoRespuestaArrayAreaConocimiento;
    }

    public void setObjetoRespuestaArrayAreaConocimiento(ArrayList<AreaConocimiento> objetoRespuestaArrayAreaConocimiento) {
        this.objetoRespuestaArrayAreaConocimiento = objetoRespuestaArrayAreaConocimiento;
    }

    public ArrayList<LineaInvestigacion> getObjetoRespuestaArrayLineaInvestigacion() {
        return objetoRespuestaArrayLineaInvestigacion;
    }

    public void setObjetoRespuestaArrayLineaInvestigacion(ArrayList<LineaInvestigacion> objetoRespuestaArrayLineaInvestigacion) {
        this.objetoRespuestaArrayLineaInvestigacion = objetoRespuestaArrayLineaInvestigacion;
    }

    public ArrayList<Institucion> getObjetoRespuestaArrayInstituciones() {
        return objetoRespuestaArrayInstituciones;
    }

    public void setObjetoRespuestaArrayInstituciones(ArrayList<Institucion> objetoRespuestaArrayInstituciones) {
        this.objetoRespuestaArrayInstituciones = objetoRespuestaArrayInstituciones;
    }

    public ArrayList<GrupoInvestigacion> getObjetoRespuestaArrayGrupoInvestigacion() {
        return objetoRespuestaArrayGrupoInvestigacion;
    }

    public void setObjetoRespuestaArrayGrupoInvestigacion(ArrayList<GrupoInvestigacion> objetoRespuestaArrayGrupoInvestigacion) {
        this.objetoRespuestaArrayGrupoInvestigacion = objetoRespuestaArrayGrupoInvestigacion;
    }

    public ArrayList<TipoPrograma> getObjetoRespuestaArrayTipoPrograma() {
        return objetoRespuestaArrayTipoPrograma;
    }

    public void setObjetoRespuestaArrayTipoPrograma(ArrayList<TipoPrograma> objetoRespuestaArrayTipoPrograma) {
        this.objetoRespuestaArrayTipoPrograma = objetoRespuestaArrayTipoPrograma;
    }

    public Investigador getObjetoRespuestaInvestigador() {
        return objetoRespuestaInvestigador;
    }

    public void setObjetoRespuestaInvestigador(Investigador objetoRespuestaInvestigador) {
        this.objetoRespuestaInvestigador = objetoRespuestaInvestigador;
    }

    public Object getObjetoRespuesta() {
        return objetoRespuesta;
    }

    public void setObjetoRespuesta(Object objetoRespuesta) {
        this.objetoRespuesta = objetoRespuesta;
    }

    public ArrayList<CursoProgramaAcademico> getObjetoRespuestaArrayCursoProgramaAcademico() {
        return objetoRespuestaArrayCursoProgramaAcademico;
    }

    public void setObjetoRespuestaArrayCursoProgramaAcademico(ArrayList<CursoProgramaAcademico> objetoRespuestaArrayCursoProgramaAcademico) {
        this.objetoRespuestaArrayCursoProgramaAcademico = objetoRespuestaArrayCursoProgramaAcademico;
    }

    public ArrayList<ProgramaAcademico> getObjetoRespuestaArrayProgramaAcademico() {
        return objetoRespuestaArrayProgramaAcademico;
    }

    public void setObjetoRespuestaArrayProgramaAcademico(ArrayList<ProgramaAcademico> objetoRespuestaArrayProgramaAcademico) {
        this.objetoRespuestaArrayProgramaAcademico = objetoRespuestaArrayProgramaAcademico;
    }

    public ArrayList<ProgramaAcademico> getObjetoRespuestaArrayProgramaAcademicoAlmacenado() {
        return objetoRespuestaArrayProgramaAcademicoAlmacenado;
    }

    public void setObjetoRespuestaArrayProgramaAcademicoAlmacenado(ArrayList<ProgramaAcademico> objetoRespuestaArrayProgramaAcademicoAlmacenado) {
        this.objetoRespuestaArrayProgramaAcademicoAlmacenado = objetoRespuestaArrayProgramaAcademicoAlmacenado;
    }

    public ArrayList<Diplomado> getObjetoRespuestaArrayDiplomadoAlmacenado() {
        return objetoRespuestaArrayDiplomadoAlmacenado;
    }

    public void setObjetoRespuestaArrayDiplomadoAlmacenado(ArrayList<Diplomado> objetoRespuestaArrayDiplomadoAlmacenado) {
        this.objetoRespuestaArrayDiplomadoAlmacenado = objetoRespuestaArrayDiplomadoAlmacenado;
    }

    public ArrayList<CursoProgramaAcademico> getObjetoRespuestaArrayCursoProgramaAcademicoAlmacenado() {
        return objetoRespuestaArrayCursoProgramaAcademicoAlmacenado;
    }

    public void setObjetoRespuestaArrayCursoProgramaAcademicoAlmacenado(ArrayList<CursoProgramaAcademico> objetoRespuestaArrayCursoProgramaAcademicoAlmacenado) {
        this.objetoRespuestaArrayCursoProgramaAcademicoAlmacenado = objetoRespuestaArrayCursoProgramaAcademicoAlmacenado;
    }

    public ArrayList<HistorialInvestigadorGrupo> getObjetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado() {
        return objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado;
    }

    public void setObjetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado(ArrayList<HistorialInvestigadorGrupo> objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado) {
        this.objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado = objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado;
    }

    public ArrayList<HistorialConferenciasInvestigador> getObjetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado() {
        return objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado;
    }

    public void setObjetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado(ArrayList<HistorialConferenciasInvestigador> objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado) {
        this.objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado = objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado;
    }

    public ArrayList<MiembrosGrupo> getObjetoRespuestaArrayMiembrosGrupo() {
        return objetoRespuestaArrayMiembrosGrupo;
    }

    public void setObjetoRespuestaArrayMiembrosGrupo(ArrayList<MiembrosGrupo> objetoRespuestaArrayMiembrosGrupo) {
        this.objetoRespuestaArrayMiembrosGrupo = objetoRespuestaArrayMiembrosGrupo;
    }
     
}
