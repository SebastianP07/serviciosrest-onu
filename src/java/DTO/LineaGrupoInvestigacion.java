package DTO;

public class LineaGrupoInvestigacion {
    private String id;
    private String idGrupoInvestigacion;
    private String idLineaInvestigacion; 

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getIdLineaInvestigacion() {
        return idLineaInvestigacion;
    }

    public void setIdLineaInvestigacion(String idLineaInvestigacion) {
        this.idLineaInvestigacion = idLineaInvestigacion;
    }
}
