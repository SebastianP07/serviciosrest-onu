package DTO;

public class LineaInvestigacion {
    private int idLineaInvestigacion;
    private String codigo;
    private String nombre; 
    private int idAreaConocimiento;
    private String nombreAreaConocimiento;

    public int getIdLineaInvestigacion() {
        return idLineaInvestigacion;
    }

    public void setIdLineaInvestigacion(int idLineaInvestigacion) {
        this.idLineaInvestigacion = idLineaInvestigacion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdAreaConocimiento() {
        return idAreaConocimiento;
    }

    public void setIdAreaConocimiento(int idAreaConocimiento) {
        this.idAreaConocimiento = idAreaConocimiento;
    }

    public String getNombreAreaConocimiento() {
        return nombreAreaConocimiento;
    }

    public void setNombreAreaConocimiento(String nombreAreaConocimiento) {
        this.nombreAreaConocimiento = nombreAreaConocimiento;
    }
    
    
}
