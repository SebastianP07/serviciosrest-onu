package DTO;

public class Instituciones {
    
    private String idInstituciones;
    private String nombre;
    private String direccion;
    private int telefono;
    private int idciudad;

    public String getIdInstituciones() {
        return idInstituciones;
    }

    public void setIdInstituciones(String idInstituciones) {
        this.idInstituciones = idInstituciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(int idciudad) {
        this.idciudad = idciudad;
    }
    
    
    
}
