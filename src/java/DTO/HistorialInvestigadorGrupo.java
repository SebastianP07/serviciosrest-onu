package DTO;

public class HistorialInvestigadorGrupo {
    private String idHistorialInvestigadorGrupo;
    private String idMiembro;
    private String actividad;
    private String fechaInicio;
    private String fechaFin;
    private String horasDedicadas;

    public String getIdHistorialInvestigadorGrupo() {
        return idHistorialInvestigadorGrupo;
    }

    public void setIdHistorialInvestigadorGrupo(String idHistorialInvestigadorGrupo) {
        this.idHistorialInvestigadorGrupo = idHistorialInvestigadorGrupo;
    }

    public String getIdMiembro() {
        return idMiembro;
    }

    public void setIdMiembro(String idMiembro) {
        this.idMiembro = idMiembro;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getHorasDedicadas() {
        return horasDedicadas;
    }

    public void setHorasDedicadas(String horasDedicadas) {
        this.horasDedicadas = horasDedicadas;
    }
    
    
}
