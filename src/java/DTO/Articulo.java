package DTO;

public class Articulo {
    private String id;
    private String titulo;
    private String fechaPresentacion; 
    private String fechaAceptacion;
    private String idSemillero;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFechaPresentacion() {
        return fechaPresentacion;
    }

    public void setFechaPresentacion(String fechaPresentacion) {
        this.fechaPresentacion = fechaPresentacion;
    }

    public String getFechaAceptacion() {
        return fechaAceptacion;
    }

    public void setFechaAceptacion(String fechaAceptacion) {
        this.fechaAceptacion = fechaAceptacion;
    }

    public String getIdSemillero() {
        return idSemillero;
    }

    public void setIdSemillero(String idSemillero) {
        this.idSemillero = idSemillero;
    }
}
