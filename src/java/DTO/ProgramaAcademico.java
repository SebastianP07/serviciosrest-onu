package DTO;

public class ProgramaAcademico {
    private String idProgramaAcademico;
    private String idGrupoInvestigacion;
    private String denominacion; 
    private String registroCalificado;
    private String registroAcreditacion;
    private String fechaPrimerCorte; 
    private String idTipoPrograma;
    private String nombre;
    private String idInstitucion;
    private TipoPrograma tipoPrograma;
    private Institucion institucion;

    public String getIdProgramaAcademico() {
        return idProgramaAcademico;
    }

    public void setIdProgramaAcademico(String idProgramaAcademico) {
        this.idProgramaAcademico = idProgramaAcademico;
    }

    public String getIdGrupoInvestigacion() {
        return idGrupoInvestigacion;
    }

    public void setIdGrupoInvestigacion(String idGrupoInvestigacion) {
        this.idGrupoInvestigacion = idGrupoInvestigacion;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public String getRegistroCalificado() {
        return registroCalificado;
    }

    public void setRegistroCalificado(String registroCalificado) {
        this.registroCalificado = registroCalificado;
    }

    public String getRegistroAcreditacion() {
        return registroAcreditacion;
    }

    public void setRegistroAcreditacion(String registroAcreditacion) {
        this.registroAcreditacion = registroAcreditacion;
    }

    public String getFechaPrimerCorte() {
        return fechaPrimerCorte;
    }

    public void setFechaPrimerCorte(String fechaPrimerCorte) {
        this.fechaPrimerCorte = fechaPrimerCorte;
    }

    public String getIdTipoPrograma() {
        return idTipoPrograma;
    }

    public void setIdTipoPrograma(String idTipoPrograma) {
        this.idTipoPrograma = idTipoPrograma;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdInstitucion() {
        return idInstitucion;
    }

    public void setIdInstitucion(String idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public TipoPrograma getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(TipoPrograma tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }

    
}
