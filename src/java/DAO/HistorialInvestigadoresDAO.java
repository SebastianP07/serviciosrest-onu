/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.ConexionBD;
import DTO.Ciudad;
import DTO.Departamento;
import DTO.HistorialConferenciasInvestigador;
import DTO.HistorialInvestigadorGrupo;
import DTO.MiembrosGrupo;
import DTO.Pais;
import DTO.ProgramaAcademico;
import DTO.Respuesta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JUAN
 */
public class HistorialInvestigadoresDAO {

    ConexionBD cn = new ConexionBD();
    
    public Respuesta verMiembrosGrupo() {
        Respuesta rta = new Respuesta();
        //ArrayList<MiembrosGrupo> objetoRespuesta = new ArrayList<MiembrosGrupo>();
        ArrayList<Integer> arreglo = new ArrayList<Integer>();
        String consultaSql = "select * from \"Dashboard-onu\".miembros_grupo order by idinvestigador asc;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                arreglo.add(rs.getInt("id"));
            }
            rta.setCodigo(1);
            rta.setIdMiembosGrupo(arreglo);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }


    public Respuesta crearHistorialInvestigador(HistorialInvestigadorGrupo historialInvestigadorGrupo) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR PROGRAMA ACADEMICO ------------------------------------");
        System.out.println(historialInvestigadorGrupo.getIdMiembro());
        System.out.println(historialInvestigadorGrupo.getActividad());
        System.out.println(historialInvestigadorGrupo.getFechaInicio());
        System.out.println(historialInvestigadorGrupo.getFechaFin());
        System.out.println(historialInvestigadorGrupo.getHorasDedicadas());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".historial_investigador_grupo\n"
                    + "(idmiembro, actividad, fechainicio, fechafin, horasdedicadas)\n"
                    + "VALUES(?, ?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(historialInvestigadorGrupo.getIdMiembro()));
            ps.setString(2, historialInvestigadorGrupo.getActividad());
            ps.setString(3, historialInvestigadorGrupo.getFechaInicio());
            ps.setString(4, historialInvestigadorGrupo.getFechaFin());
            ps.setInt(5, Integer.parseInt(historialInvestigadorGrupo.getHorasDedicadas()));

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar crearHistorialInvestigador: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearHistorialInvestigador: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta crearHistorialConferencias(HistorialConferenciasInvestigador historialConferenciasInvestigador) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR PROGRAMA ACADEMICO ------------------------------------");
        System.out.println(historialConferenciasInvestigador.getIdMiembro());
        System.out.println(historialConferenciasInvestigador.getFecha());
        System.out.println(historialConferenciasInvestigador.getIdCiudad());
        //System.out.println(historialConferenciasInvestigador.getCopiaCertificado());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".conferencia\n"
                    + "(idmiembro, fecha, idciudad)\n"
                    + "VALUES(?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(historialConferenciasInvestigador.getIdMiembro()));
            ps.setString(2, historialConferenciasInvestigador.getFecha());
            ps.setInt(3, Integer.parseInt(historialConferenciasInvestigador.getIdCiudad()));

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar crearHistorialConferencias: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearHistorialConferencias: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verHistorialInvestigador(Respuesta rta) {
        ArrayList<HistorialInvestigadorGrupo> objetoRespuesta = new ArrayList<HistorialInvestigadorGrupo>();
        String consultaSql = "select * from \"Dashboard-onu\".vista_historicoactividadesgrupo;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HistorialInvestigadorGrupo historialInvestigadorGrupo = new HistorialInvestigadorGrupo();

                historialInvestigadorGrupo.setIdHistorialInvestigadorGrupo(String.valueOf(rs.getInt("id_historial_investigador_grupo")));
                historialInvestigadorGrupo.setIdMiembro(String.valueOf(rs.getInt("id_miembro")));
                historialInvestigadorGrupo.setActividad(rs.getString("actividad"));
                historialInvestigadorGrupo.setFechaInicio(rs.getString("fechainicio"));
                historialInvestigadorGrupo.setFechaFin(rs.getString("fechafin"));
                historialInvestigadorGrupo.setHorasDedicadas(rs.getString("horasdedicadas"));

                objetoRespuesta.add(historialInvestigadorGrupo);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verHistorialConferencias(Respuesta rta) {
        ArrayList<HistorialConferenciasInvestigador> objetoRespuesta = new ArrayList<HistorialConferenciasInvestigador>();
        String consultaSql = "select * from \"Dashboard-onu\".vista_conferencias";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                HistorialConferenciasInvestigador historialConferenciasInvestigador = new HistorialConferenciasInvestigador();

                historialConferenciasInvestigador.setIdHistorialConferenciasInvestigador(String.valueOf(rs.getInt("id_conferencia")));
                historialConferenciasInvestigador.setIdMiembro(String.valueOf(rs.getInt("id_miembro")));

                Ciudad ciudad = new Ciudad();
                ciudad.setIdCiudad(rs.getInt("id_ciudad"));
                ciudad.setCiudad(rs.getString("ciudad"));
                historialConferenciasInvestigador.setCiudad(ciudad);

                Departamento departamento = new Departamento();
                departamento.setId(String.valueOf(rs.getInt("id_departamento")));
                departamento.setDepartamento(rs.getString("estado"));
                historialConferenciasInvestigador.setDepartamento(departamento);

                Pais pais = new Pais();
                pais.setId(String.valueOf(rs.getInt("id_pais")));
                pais.setPais(rs.getString("pais"));
                historialConferenciasInvestigador.setPais(pais);

                historialConferenciasInvestigador.setFecha(rs.getString("fecha_realizacion"));

                objetoRespuesta.add(historialConferenciasInvestigador);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

}
