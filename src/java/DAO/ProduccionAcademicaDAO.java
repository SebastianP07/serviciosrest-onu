package DAO;

import Conexion.ConexionBD;
import DTO.AreaConocimiento;
import DTO.CursoProgramaAcademico;
import DTO.Diplomado;
import DTO.Institucion;
import DTO.Investigador;
import DTO.ProgramaAcademico;
import DTO.Respuesta;
import DTO.TipoPrograma;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProduccionAcademicaDAO {

    ConexionBD cn = new ConexionBD();

    public Respuesta crearProgramaAcademico(ProgramaAcademico programaAcademico) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR PROGRAMA ACADEMICO ------------------------------------");
        System.out.println(programaAcademico.getIdGrupoInvestigacion());
        System.out.println(programaAcademico.getDenominacion());
        System.out.println(programaAcademico.getRegistroCalificado());
        System.out.println(programaAcademico.getRegistroAcreditacion());
        System.out.println(programaAcademico.getFechaPrimerCorte());
        System.out.println(programaAcademico.getIdTipoPrograma());
        System.out.println(programaAcademico.getNombre());
        System.out.println(programaAcademico.getIdInstitucion());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".programa_academico\n"
                    + "(idgrupoinvestigacion, denominacion, registrocalificado, registroacreditacion, fechaprimercorte, idtipoprograma, nombre, idinstitucion)\n"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(programaAcademico.getIdGrupoInvestigacion()));
            ps.setString(2, programaAcademico.getDenominacion());
            ps.setString(3, programaAcademico.getRegistroCalificado());
            ps.setString(4, programaAcademico.getRegistroAcreditacion());
            ps.setString(5, programaAcademico.getFechaPrimerCorte());
            ps.setInt(6, Integer.parseInt(programaAcademico.getIdTipoPrograma()));
            ps.setString(7, programaAcademico.getNombre());
            ps.setInt(8, Integer.parseInt(programaAcademico.getIdInstitucion()));

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar crearProgramaAcademico: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearProgramaAcademico: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta crearDiplomado(Diplomado diplomado) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR DIPLOMADO ------------------------------------");
        System.out.println(diplomado.getIdGrupoInvestigacion());
        System.out.println(diplomado.getIdAreaConocimiento());
        System.out.println(diplomado.getDescripcion());
        System.out.println(diplomado.getFechaPrimerCorte());
        System.out.println(diplomado.getIdInstitucion());
        System.out.println(diplomado.getNombre());
        System.out.println(diplomado.getDenominacion());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".diplomado\n"
                    + "(idgrupoinvestigacion, idareaconocimiento, descripcion, fechaprimercorte, idinstitucion, nombre, denominacion)\n"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(diplomado.getIdGrupoInvestigacion()));
            ps.setInt(2, Integer.parseInt(diplomado.getIdAreaConocimiento()));
            ps.setString(3, diplomado.getDescripcion());
            ps.setString(4, diplomado.getFechaPrimerCorte());
            ps.setInt(5, Integer.parseInt(diplomado.getIdInstitucion()));
            ps.setString(6, diplomado.getNombre());
            ps.setString(7, diplomado.getDenominacion());

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar crearDiplomado: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearDiplomado: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta crearCursoProgramaAcademico(CursoProgramaAcademico cursoProgramaAcademico) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR CURSO PROGRAMA ACADEMICO ------------------------------------");
        System.out.println(cursoProgramaAcademico.getIdProgramaAcademico());
        System.out.println(cursoProgramaAcademico.getIdAreaConocimiento());
        System.out.println(cursoProgramaAcademico.getNombre());
        System.out.println(cursoProgramaAcademico.getDenominacion());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".curso_programa_academico\n"
                    + "(idprogramaacademico, idareaconocimiento, nombre, denominacion)\n"
                    + "VALUES(?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(cursoProgramaAcademico.getIdProgramaAcademico()));
            ps.setInt(2, Integer.parseInt(cursoProgramaAcademico.getIdAreaConocimiento()));
            ps.setString(3, cursoProgramaAcademico.getNombre());
            ps.setString(4, cursoProgramaAcademico.getDenominacion());

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar crearCursoProgramaAcademico: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearCursoProgramaAcademico: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verProgramasAcademicos() {
        Respuesta rta = new Respuesta();
        ArrayList<ProgramaAcademico> objetoRespuesta = new ArrayList<ProgramaAcademico>();
        String consultaSql = "select * from \"Dashboard-onu\".programa_academico";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProgramaAcademico programaAcademico = new ProgramaAcademico();
                programaAcademico.setIdProgramaAcademico(String.valueOf(rs.getInt("id")));
                programaAcademico.setIdGrupoInvestigacion(String.valueOf(rs.getInt("idgrupoinvestigacion")));
                programaAcademico.setDenominacion(rs.getString("denominacion"));
                programaAcademico.setRegistroCalificado(rs.getString("registrocalificado"));
                programaAcademico.setRegistroAcreditacion(rs.getString("registroacreditacion"));
                programaAcademico.setFechaPrimerCorte(rs.getString("fechaprimercorte"));
                programaAcademico.setIdTipoPrograma(String.valueOf(rs.getInt("idtipoprograma")));
                programaAcademico.setNombre(rs.getString("nombre"));
                programaAcademico.setIdInstitucion(String.valueOf(rs.getInt("idinstitucion")));
                objetoRespuesta.add(programaAcademico);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayProgramaAcademico(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verProgramasAcademicosAlmacenados(Respuesta rta){
        ArrayList<ProgramaAcademico> objetoRespuesta = new ArrayList<ProgramaAcademico>();
        String consultaSql = "select * from \"Dashboard-onu\".vista_programas_academicos order by id_programa_academico;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ProgramaAcademico programaAcademico = new ProgramaAcademico();
                programaAcademico.setIdProgramaAcademico(String.valueOf(rs.getInt("id_programa_academico")));
                programaAcademico.setNombre(rs.getString("programa_academico"));
                programaAcademico.setIdGrupoInvestigacion(String.valueOf(rs.getInt("id_grupo_investigacion")));
                
                TipoPrograma tipoPrograma = new TipoPrograma();
                tipoPrograma.setIdTipoPrograma(String.valueOf(rs.getInt("id_tipo_programa")));
                tipoPrograma.setTipoProgramaNombre(rs.getString("tipo_programa"));
                programaAcademico.setTipoPrograma(tipoPrograma);
                
                Institucion institucion = new Institucion();
                institucion.setIdInstitucion(rs.getInt("id_institucion"));
                institucion.setNombre(rs.getString("institucion"));
                programaAcademico.setInstitucion(institucion);
                
                programaAcademico.setDenominacion(rs.getString("denominacion"));
                programaAcademico.setRegistroCalificado(rs.getString("registro_calificado"));
                programaAcademico.setRegistroAcreditacion(rs.getString("registro_acreditacion"));
                programaAcademico.setFechaPrimerCorte(rs.getString("fecha_primer_corte"));
                objetoRespuesta.add(programaAcademico);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayProgramaAcademicoAlmacenado(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }
    
    public Respuesta verDiplomadosAlmacenados(Respuesta rta){
        ArrayList<Diplomado> objetoRespuesta = new ArrayList<Diplomado>();
        String consultaSql = "select * from \"Dashboard-onu\".vista_diplomados order by id_diplomado;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Diplomado diplomado = new Diplomado();
                diplomado.setIdDiplomado(String.valueOf(rs.getInt("id_diplomado")));
                diplomado.setNombre(rs.getString("diplomado"));
                diplomado.setIdGrupoInvestigacion(String.valueOf(rs.getInt("id_diplomado")));
                
                AreaConocimiento areaConocimiento = new AreaConocimiento();
                areaConocimiento.setIdAreaConocimiento(rs.getInt("id_area_conocimiento"));
                areaConocimiento.setNombre(rs.getString("area_conocimiento"));
                diplomado.setAreaConocimiento(areaConocimiento);
                
                Institucion institucion = new Institucion();
                institucion.setIdInstitucion(rs.getInt("id_institucion"));
                institucion.setNombre(rs.getString("institucion"));
                diplomado.setInstitucion(institucion);
                
                diplomado.setDenominacion(rs.getString("denominacion"));
                diplomado.setDescripcion(rs.getString("descripcion"));
                diplomado.setFechaPrimerCorte(rs.getString("fecha_primer_corte"));
                objetoRespuesta.add(diplomado);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayDiplomadoAlmacenado(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }
    
    public Respuesta verCursosProgramaAcademicoAlmacenados(Respuesta rta){
        ArrayList<CursoProgramaAcademico> objetoRespuesta = new ArrayList<CursoProgramaAcademico>();
        String consultaSql = "select * from \"Dashboard-onu\".vista_cursos order by id_curso;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CursoProgramaAcademico cursoProgramaAcademico = new CursoProgramaAcademico();
                cursoProgramaAcademico.setIdCursoProgramaAcademico(String.valueOf(rs.getInt("id_curso")));
                cursoProgramaAcademico.setNombre(rs.getString("curso"));
                cursoProgramaAcademico.setDenominacion(rs.getString("denominacion"));
                
                ProgramaAcademico programaAcademico = new ProgramaAcademico();
                programaAcademico.setIdProgramaAcademico(String.valueOf(rs.getInt("id_programa_academico")));
                programaAcademico.setNombre(rs.getString("programa_academico"));
                cursoProgramaAcademico.setProgramaAcademico(programaAcademico);
                
                TipoPrograma tipoPrograma = new TipoPrograma();
                tipoPrograma.setIdTipoPrograma(String.valueOf(rs.getInt("id_tipo_programa")));
                tipoPrograma.setTipoProgramaNombre(rs.getString("tipo_programa"));
                cursoProgramaAcademico.setTipoPrograma(tipoPrograma);
                
                AreaConocimiento areaConocimiento = new AreaConocimiento();
                areaConocimiento.setIdAreaConocimiento(rs.getInt("id_area_conocimiento"));
                areaConocimiento.setNombre(rs.getString("area_conocimiento"));
                cursoProgramaAcademico.setAreaConocimiento(areaConocimiento);
                
                Institucion institucion = new Institucion();
                institucion.setIdInstitucion(rs.getInt("id_institucion"));
                institucion.setNombre(rs.getString("institucion"));
                cursoProgramaAcademico.setInstitucion(institucion);
                
                objetoRespuesta.add(cursoProgramaAcademico);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayCursoProgramaAcademicoAlmacenado(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }
}
