package DAO;

import Conexion.ConexionBD;
import DTO.Investigador;
import DTO.Respuesta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutenticacionDAO {

    ConexionBD cn = new ConexionBD();
    
    public Respuesta registrarUsuario(Investigador investigador) {
        Respuesta rta = new Respuesta();
        /*
        System.out.println(" REGISTRAR USUARIO ***********************************");

        System.out.println(investigador.getNumeroidentificacion());
        System.out.println(investigador.getPrimerapellido());
        System.out.println(investigador.getSegundoapellido());
        System.out.println(investigador.getNombre());
        System.out.println(investigador.getGenero());
        System.out.println(investigador.getFechanacimiento());
        System.out.println(investigador.getFechainscripcion());
        System.out.println(investigador.getDireccion());
        System.out.println(investigador.getTelefono());
        System.out.println(investigador.getCorreoElectronico());
        System.out.println(investigador.getPassword());
        */
        try {
            String sql = "INSERT INTO \"Dashboard-onu\".investigador\n"
                    + "(numeroidentificacion, primerapellido, segundoapellido, nombre, genero, fechanacimiento, fechainscripcion, direccion, telefono, correoelectronico, \"password\")\n"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setDouble(1, Double.parseDouble(investigador.getNumeroidentificacion()));
            ps.setString(2, investigador.getPrimerapellido());
            ps.setString(3, investigador.getSegundoapellido());
            ps.setString(4, investigador.getNombre());
            ps.setString(5, investigador.getGenero());
            ps.setString(6, investigador.getFechanacimiento());
            ps.setString(7, investigador.getFechainscripcion());
            ps.setString(8, investigador.getDireccion());
            ps.setString(9, investigador.getTelefono());
            ps.setString(10, investigador.getCorreoElectronico());
            ps.setString(11, investigador.getPassword());

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito, por favor inicie sesión.");

        } catch (SQLException e) {
            System.out.println("Error al Insertar registrarUsuario: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase registrarUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta iniciarSesionDAO(Investigador investigador) {
        Respuesta rta = new Respuesta();
        Investigador investigadorRta = null;

        try {
            String consultaSql = "SELECT * FROM \"Dashboard-onu\".investigador\n"
                    + "where correoelectronico = ?\n"
                    + "and password  = ?;";
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ps.setString(1, investigador.getCorreoElectronico());
            ps.setString(2, investigador.getPassword());

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                investigadorRta = new Investigador();
                investigadorRta.setNumeroidentificacion(rs.getString("numeroidentificacion"));
                investigadorRta.setPrimerapellido(rs.getString("primerapellido"));
                investigadorRta.setSegundoapellido(rs.getString("segundoapellido"));
                investigadorRta.setNombre(rs.getString("nombre"));
                investigadorRta.setGenero(rs.getString("genero"));
                investigadorRta.setFechanacimiento(String.valueOf(rs.getString("fechanacimiento")));
                investigadorRta.setFechainscripcion(String.valueOf(rs.getString("fechainscripcion")));
                investigadorRta.setDireccion(rs.getString("direccion"));
                investigadorRta.setTelefono(rs.getString("telefono"));
                investigadorRta.setCorreoElectronico(rs.getString("correoElectronico"));
            }

            if (investigadorRta != null) {
                rta.setCodigo(1);
                rta.setDescripcion("Consulta Exitosa.");
                rta.setObjetoRespuestaInvestigador(investigadorRta);
            } else {
                rta.setCodigo(2);
                rta.setDescripcion("El usuario no existe, por favor registrese.");
            }
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error al iniciar sesión, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase iniciarSesionDAO: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

}
