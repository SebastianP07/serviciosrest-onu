package DAO;

import Conexion.ConexionBD;
import DTO.AreaConocimiento;
import DTO.Ciudad;
import DTO.GrupoInvestigacion;
import DTO.Investigador;
import DTO.MiembrosGrupo;
import DTO.Respuesta;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class GrupoInvestigacionDAO {

    ConexionBD cn = new ConexionBD();

    public Respuesta crearGrupoInvestigacion(GrupoInvestigacion grupoInvestigacion) {
        Respuesta rta = new Respuesta();

        System.out.println("*****************************************************");
        System.out.println(grupoInvestigacion.getPaginaWeb());
        System.out.println(grupoInvestigacion.getEmail());
        System.out.println(grupoInvestigacion.getIdAreaConocimiento());
        System.out.println(grupoInvestigacion.getIdCiudad());
        System.out.println(grupoInvestigacion.getMesConformacion());
        System.out.println(grupoInvestigacion.getAnioConformacion());
        System.out.println(grupoInvestigacion.getInstituciones().getIdInstituciones());
        System.out.println("LIDER --------------------------");
        System.out.println(grupoInvestigacion.getMiembrosGrupoLider().getFechaVinculacion());
        System.out.println(grupoInvestigacion.getMiembrosGrupoLider().getIdInvestigador());
        System.out.println(grupoInvestigacion.getMiembrosGrupoLider().getIdRol());
        System.out.println("MIEMBRO --------------------------");
        System.out.println(grupoInvestigacion.getMiembrosGrupo().getFechaVinculacion());
        System.out.println(grupoInvestigacion.getMiembrosGrupo().getIdInvestigador());
        System.out.println(grupoInvestigacion.getMiembrosGrupo().getIdRol());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".grupo_investigacion\n"
                    + "(paginaweb, email, idareaconocimiento, idciudad, mesconformacion, anioconformacion)\n"
                    + "VALUES(?, ?, ?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            ps.setString(1, grupoInvestigacion.getPaginaWeb());
            ps.setString(2, grupoInvestigacion.getEmail());
            ps.setInt(3, Integer.parseInt(grupoInvestigacion.getIdAreaConocimiento()));
            ps.setInt(4, Integer.parseInt(grupoInvestigacion.getIdCiudad()));
            ps.setString(5, grupoInvestigacion.getMesConformacion());
            ps.setString(6, grupoInvestigacion.getAnioConformacion());

            ps.executeUpdate();

            rta.setCodigo(1);
            rta.setDescripcion("Se inserto el registro con éxito.");
        } catch (SQLException e) {
            System.out.println("Error al Insertar registrarUsuario: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearGrupoInvestigacion INSERT INTO \"Dashboard-onu\".grupo_investigacion: " + e);
        } finally {
            cn.desconectarse();
        }

        if (rta.getCodigo() == 1) {
            try {
                String sql = "SELECT *\n"
                        + "FROM \"Dashboard-onu\".grupo_investigacion\n"
                        + "where paginaweb = ?\n"
                        + "and email = ?\n"
                        + "and idareaconocimiento = ?\n"
                        + "and idciudad = ?\n"
                        + "and mesconformacion = ?\n"
                        + "and anioconformacion = ?;";

                PreparedStatement ps = cn.conectar().prepareCall(sql);
                ps.setString(1, grupoInvestigacion.getPaginaWeb());
                ps.setString(2, grupoInvestigacion.getEmail());
                ps.setInt(3, Integer.parseInt(grupoInvestigacion.getIdAreaConocimiento()));
                ps.setInt(4, Integer.parseInt(grupoInvestigacion.getIdCiudad()));
                ps.setString(5, grupoInvestigacion.getMesConformacion());
                ps.setString(6, grupoInvestigacion.getAnioConformacion());

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    grupoInvestigacion.getMiembrosGrupo().setIdGrupoInvestigacion(rs.getString("id"));
                    System.out.println("ID -------->>>>" + rs.getString("id"));
                }

                if (grupoInvestigacion.getMiembrosGrupo().getIdGrupoInvestigacion() != null) {
                    rta.setCodigo(1);
                    rta.setObjetoRespuesta(grupoInvestigacion);
                } else {
                    rta.setCodigo(0);
                    rta.setDescripcion("Error al obtenerl el ID de la tabla");
                }
            } catch (SQLException e) {
                System.out.println("Error al Insertar registrarUsuario: " + e);
                rta.setCodigo(0);
                rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
                rta.setDescripcionError("Error clase registrarUsuario SELECT \"Dashboard-onu\".grupo_investigacion: " + e);
            } finally {
                cn.desconectarse();
            }
        }
        return rta;
    }

    public Respuesta crearGrupoInvestigacionMiembrosGrupo(GrupoInvestigacion grupoInvestigacionRta) {
        Respuesta rta = new Respuesta();
        String idTablaGrupoInvestigacion = grupoInvestigacionRta.getMiembrosGrupo().getIdGrupoInvestigacion();

        System.out.println("CREAR MIEMBROS ************************");
        System.out.println(idTablaGrupoInvestigacion);
        System.out.println("Grupo");
        System.out.println(grupoInvestigacionRta.getMiembrosGrupo().getIdInvestigador());
        System.out.println(grupoInvestigacionRta.getMiembrosGrupo().getIdRol());
        System.out.println(grupoInvestigacionRta.getMiembrosGrupo().getFechaVinculacion());
        System.out.println("Lider");
        System.out.println(grupoInvestigacionRta.getMiembrosGrupoLider().getIdInvestigador());
        System.out.println(grupoInvestigacionRta.getMiembrosGrupoLider().getIdRol());
        System.out.println(grupoInvestigacionRta.getMiembrosGrupoLider().getFechaVinculacion());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".miembros_grupo\n"
                    + "(idinvestigador, idrol, idgrupoinvestigacion, fechavinculacion)\n"
                    + "VALUES(?, ?, ?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);
            System.out.println("Lider ++++");

            // INSERT DEL LIDER
            ps.setInt(1, Integer.parseInt(grupoInvestigacionRta.getMiembrosGrupoLider().getIdInvestigador()));
            ps.setInt(2, Integer.parseInt(grupoInvestigacionRta.getMiembrosGrupoLider().getIdRol()));
            ps.setInt(3, Integer.parseInt(idTablaGrupoInvestigacion));
            ps.setString(4, grupoInvestigacionRta.getMiembrosGrupoLider().getFechaVinculacion());
            ps.executeUpdate();

            rta.setCodigo(1);
        } catch (SQLException e) {
            System.out.println("Error al Insertar crearGrupoInvestigacionMiembrosGrupo: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearGrupoInvestigacionMiembrosGrupo: " + e);
        } finally {
            cn.desconectarse();
        }

        if (rta.getCodigo() == 1) {
            try {
                String sql = "INSERT INTO \"Dashboard-onu\".miembros_grupo\n"
                        + "(idinvestigador, idrol, idgrupoinvestigacion, fechavinculacion)\n"
                        + "VALUES(?, ?, ?, ?);";

                String arrayIdGrupo[] = grupoInvestigacionRta.getMiembrosGrupo().getIdInvestigador().split(",");

                PreparedStatement ps = cn.conectar().prepareStatement(sql);
                System.out.println("Grupo ++++ length:" + arrayIdGrupo.length);

                // INSERT DEL GRUPO
                for (int i = 0; i < arrayIdGrupo.length; i++) {
                    ps.setInt(1, Integer.parseInt(arrayIdGrupo[i]));
                    ps.setInt(2, Integer.parseInt(grupoInvestigacionRta.getMiembrosGrupo().getIdRol()));
                    ps.setInt(3, Integer.parseInt(idTablaGrupoInvestigacion));
                    ps.setString(4, grupoInvestigacionRta.getMiembrosGrupo().getFechaVinculacion());
                    ps.addBatch();
                }
                ps.executeBatch();
                ps.close();

                rta.setCodigo(1);
                rta.setDescripcion("Se inserto el registro con éxito.");
                rta.setObjetoRespuesta(grupoInvestigacionRta);
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Error al Insertar crearGrupoInvestigacionMiembrosGrupo: " + e);
                rta.setCodigo(0);
                rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
                rta.setDescripcionError("Error clase crearGrupoInvestigacionMiembrosGrupo: " + e);
            } finally {
                cn.desconectarse();
            }
        }
        return rta;
    }

    public Respuesta crearGrupoInvestigacionInstitucionesGrupo(GrupoInvestigacion grupoInvestigacionRta2) {
        Respuesta rta = new Respuesta();

        System.out.println("CREAR INSTITUCIONES ************************");
        System.out.println(grupoInvestigacionRta2.getMiembrosGrupo().getIdGrupoInvestigacion());
        System.out.println(grupoInvestigacionRta2.getInstituciones().getIdInstituciones());

        try {
            String sql = "INSERT INTO \"Dashboard-onu\".instituciones_grupo\n"
                    + "(idgrupoinvestigacion, idinstitucion)\n"
                    + "VALUES(?, ?);";

            PreparedStatement ps = cn.conectar().prepareStatement(sql);

            String arrayIdInstituciones[] = grupoInvestigacionRta2.getInstituciones().getIdInstituciones().split(",");

            for (int i = 0; i < arrayIdInstituciones.length; i++) {
                ps.setInt(1, Integer.parseInt(grupoInvestigacionRta2.getMiembrosGrupo().getIdGrupoInvestigacion()));
                ps.setInt(2, Integer.parseInt(arrayIdInstituciones[i]));
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();

            rta.setCodigo(1);
            rta.setDescripcion("Se registro con éxito el Grupo.");
            rta.setObjetoRespuesta(grupoInvestigacionRta2);
        } catch (SQLException e) {
            System.out.println("Error al Insertar crearGrupoInvestigacionInstitucionesGrupo: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al insertar el registro, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase crearGrupoInvestigacionInstitucionesGrupo: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verGrupoInvestigacion() {
        Respuesta rta = new Respuesta();
        ArrayList<GrupoInvestigacion> listaGrupos = new ArrayList<>();
        try {
            String sql = "select * from \"Dashboard-onu\".vista_grupos order by id_grupo_investigacion;";

            PreparedStatement ps = cn.conectar().prepareCall(sql);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                GrupoInvestigacion grupoInvestigacion = new GrupoInvestigacion();
                grupoInvestigacion.setIdGrupoInvestigacion(String.valueOf(rs.getInt("id_grupo_investigacion")));
                grupoInvestigacion.setPaginaWeb(rs.getString("pagina_web"));
                grupoInvestigacion.setEmail(rs.getString("email"));
                grupoInvestigacion.setMesConformacion(rs.getString("mes_conformacion"));
                grupoInvestigacion.setAnioConformacion(rs.getString("año_conformacion"));

                AreaConocimiento areaConocimiento = new AreaConocimiento();
                areaConocimiento.setIdAreaConocimiento(rs.getInt("id_area_conocimiento"));
                areaConocimiento.setNombre(rs.getString("area_conocimiento"));
                grupoInvestigacion.setAreaConocimiento(areaConocimiento);

                Ciudad ciudad = new Ciudad();
                ciudad.setIdCiudad(rs.getInt("id_ciudad"));
                ciudad.setCiudad(rs.getString("ciudad"));
                grupoInvestigacion.setCiudad(ciudad);
                
                listaGrupos.add(grupoInvestigacion);
            }

            if (listaGrupos.size() > 0) {
                rta.setCodigo(1);
                rta.setDescripcion("Consulta realizada con exito.");
                rta.setObjetoRespuestaArrayGrupoInvestigacion(listaGrupos);
            } else {
                rta.setCodigo(0);
                rta.setDescripcion("Error al consultar los grupos registrados.");
            }
        } catch (SQLException e) {
            System.out.println("Error al Insertar verGrupoInvestigacion: " + e);
            rta.setCodigo(0);
            rta.setDescripcion("Error al consultar los grupos registrados, por favor contacte con soporte.");
            rta.setDescripcionError("Error clase verGrupoInvestigacion SELECT \"Dashboard-onu\".vista_grupos: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }
}
