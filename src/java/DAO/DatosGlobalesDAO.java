package DAO;

import Conexion.ConexionBD;
import DTO.AreaConocimiento;
import DTO.Ciudad;
import DTO.CursoProgramaAcademico;
import DTO.Institucion;
import DTO.LineaInvestigacion;
import DTO.ProgramaAcademico;
import DTO.Respuesta;
import DTO.TipoPrograma;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatosGlobalesDAO {

    ConexionBD cn = new ConexionBD();

    public Respuesta obtenerDatosCiudadesDAO(Respuesta rta) {
        ArrayList<Ciudad> objetoRespuesta = new ArrayList<Ciudad>();
        String consultaSql = "SELECT id_ciudad, ciudad, id_departamento, departamento, id_pais, pais\n"
                + "FROM \"Dashboard-onu\".vista_obtenerciudades;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Ciudad ciudad = new Ciudad();
                ciudad.setIdCiudad(rs.getInt("id_ciudad"));
                ciudad.setCiudad(rs.getString("ciudad"));
                ciudad.setIdDepartamento(rs.getInt("id_departamento"));
                ciudad.setDepartamento(rs.getString("departamento"));
                ciudad.setIdPais(rs.getInt("id_pais"));
                ciudad.setPais(rs.getString("pais"));
                objetoRespuesta.add(ciudad);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayCiudades(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta obtenerDatosInstitucionesDAO(Respuesta rta) {
        ArrayList<Institucion> objetoRespuesta = new ArrayList<Institucion>();
        String consultaSql = "select * from \"Dashboard-onu\".institucion;";

        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Institucion institucion = new Institucion();
                institucion.setIdInstitucion(rs.getInt("id"));
                institucion.setNombre(rs.getString("nombre"));
                institucion.setDireccion(rs.getString("direccion"));
                institucion.setTelefono(rs.getInt("telefono"));
                institucion.setIdciudad(rs.getInt("idciudad"));
                objetoRespuesta.add(institucion);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayInstituciones(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta obtenerDatosAreaConocimientoDAO(Respuesta rta) {
        ArrayList<AreaConocimiento> objetoRespuesta = new ArrayList<AreaConocimiento>();
        String consultaSql = "select * from \"Dashboard-onu\".area_conocimiento;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AreaConocimiento areaConocimiento = new AreaConocimiento();
                areaConocimiento.setIdAreaConocimiento(rs.getInt("id"));
                areaConocimiento.setCodigo(rs.getString("codigo"));
                areaConocimiento.setNombre(rs.getString("nombre"));
                objetoRespuesta.add(areaConocimiento);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayAreaConocimiento(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta obtenerDatosLineaInvestigacionDAO(Respuesta rta) {
        ArrayList<LineaInvestigacion> objetoRespuesta = new ArrayList<LineaInvestigacion>();
        String consultaSql = "SELECT id, codigo, nombre, idareaconocimiento, nombreareaconocimiento\n"
                + "FROM \"Dashboard-onu\".vista_lineainvestigacion;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                LineaInvestigacion lineaInvestigacion = new LineaInvestigacion();
                lineaInvestigacion.setIdLineaInvestigacion(rs.getInt("id"));
                lineaInvestigacion.setCodigo(rs.getString("codigo"));
                lineaInvestigacion.setNombre(rs.getString("nombre"));
                lineaInvestigacion.setIdAreaConocimiento(rs.getInt("idareaconocimiento"));
                lineaInvestigacion.setNombreAreaConocimiento(rs.getString("nombreareaconocimiento"));
                objetoRespuesta.add(lineaInvestigacion);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayLineaInvestigacion(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta obtenerDatosTipoProgramaDAO(Respuesta rta) {
        ArrayList<TipoPrograma> objetoRespuesta = new ArrayList<TipoPrograma>();
        String consultaSql = "select * from \"Dashboard-onu\".tipo_programa;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TipoPrograma tipoPrograma = new TipoPrograma();
                tipoPrograma.setIdTipoPrograma(String.valueOf(rs.getInt("id")));
                tipoPrograma.setTipoProgramaNombre(rs.getString("tipoprograma"));
                objetoRespuesta.add(tipoPrograma);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayTipoPrograma(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }

    public Respuesta verCursos() {
        Respuesta rta = new Respuesta();
        ArrayList<CursoProgramaAcademico> objetoRespuesta = new ArrayList<CursoProgramaAcademico>();
        String consultaSql = "SELECT id_curso, curso, denominacion, id_programa_academico, programa_academico, id_tipo_programa, tipo_programa, id_area_conocimiento, area_conocimiento, id_institucion, institucion\n"
                + "FROM \"Dashboard-onu\".vista_cursos;";
        try {
            PreparedStatement ps = cn.conectar().prepareStatement(consultaSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CursoProgramaAcademico cursoProgramAcademico = new CursoProgramaAcademico();
                cursoProgramAcademico.setIdCursoProgramaAcademico(String.valueOf(rs.getInt("id_curso")));
                cursoProgramAcademico.setNombre(rs.getString("curso"));
                cursoProgramAcademico.setDenominacion(rs.getString("denominacion"));
                
                ProgramaAcademico programaAcademico = new ProgramaAcademico();
                programaAcademico.setIdProgramaAcademico(String.valueOf(rs.getInt("id_programa_academico")));
                programaAcademico.setNombre(rs.getString("programa_academico"));
                cursoProgramAcademico.setProgramaAcademico(programaAcademico);
                
                TipoPrograma tipoPrograma = new TipoPrograma();
                tipoPrograma.setIdTipoPrograma(String.valueOf(rs.getInt("id_tipo_programa")));
                tipoPrograma.setTipoProgramaNombre(rs.getString("tipo_programa"));
                
                AreaConocimiento areaConocimiento = new AreaConocimiento();
                areaConocimiento.setIdAreaConocimiento(rs.getInt("id_area_conocimiento"));
                areaConocimiento.setNombre(rs.getString("area_conocimiento"));
                cursoProgramAcademico.setAreaConocimiento(areaConocimiento);
                
                Institucion instirucion = new Institucion();
                instirucion.setIdInstitucion(rs.getInt("id_institucion"));
                instirucion.setNombre(rs.getString("institucion"));
                
                objetoRespuesta.add(cursoProgramAcademico);
            }
            rta.setCodigo(1);
            rta.setDescripcion("Consulta Exitosa.");
            rta.setObjetoRespuestaArrayCursoProgramaAcademico(objetoRespuesta);
        } catch (SQLException e) {
            rta.setCodigo(0);
            rta.setDescripcion("Error obtenerUsuario: " + e);
        } finally {
            cn.desconectarse();
        }
        return rta;
    }
}
