package REST;

import DTO.CursoProgramaAcademico;
import DTO.Diplomado;
import DTO.GrupoInvestigacion;
import DTO.HistorialConferenciasInvestigador;
import DTO.HistorialInvestigadorGrupo;
import DTO.Respuesta;
import DTO.Investigador;
import DTO.ProgramaAcademico;
import Modelo.AutenticacionModelo;
import Modelo.DatosGlobalesModelo;
import Modelo.GrupoInvestigacionModelo;
import Modelo.HistorialInvestigadoresModelo;
import Modelo.ProduccionAcademicaModelo;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("restServices")
public class RestServices {

    @POST
    @Path("registrarUsuario")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta registrarUsuario(Investigador investigador) {
        return new AutenticacionModelo().registrarUsuario(investigador);
    }

    @POST
    @Path("iniciarSesion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta iniciarSesion(Investigador investigador) {
        return new AutenticacionModelo().iniciarSesion(investigador);
    }

    @GET
    @Path("obtenerDatosGlobales")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta obtenerDatosGlobales() {
        return new DatosGlobalesModelo().obtenerDatosGlobalesModelo();
    }

    @POST
    @Path("crearGrupoInvestigacion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearGrupoInvestigacion(GrupoInvestigacion grupoInvestigacion) {
        return new GrupoInvestigacionModelo().crearGrupoInvestigacion(grupoInvestigacion);
    }

    @GET
    @Path("verGrupoInvestigacion")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verGrupoInvestigacion() {
        return new GrupoInvestigacionModelo().verGrupoInvestigacion();
    }
    
    @POST
    @Path("crearProgramaAcademico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearProgramaAcademico(ProgramaAcademico programaAcademico) {
        return new ProduccionAcademicaModelo().crearProgramaAcademico(programaAcademico);
    }
    
    @POST
    @Path("crearDiplomado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearDiplomado(Diplomado diplomado) {
        return new ProduccionAcademicaModelo().crearDiplomado(diplomado);
    }
    
    @POST
    @Path("crearCursoProgramaAcademico")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearCursoProgramaAcademico(CursoProgramaAcademico cursoProgramaAcademico) {
        return new ProduccionAcademicaModelo().crearCursoProgramaAcademico(cursoProgramaAcademico);
    }
    
    @GET
    @Path("verCursos")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verCursos() {
        return new DatosGlobalesModelo().verCursos();
    }
    
    @GET
    @Path("verProgramasAcademicos")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verProgramasAcademicos() {
        return new ProduccionAcademicaModelo().verProgramasAcademicos();
    }
    
    @GET
    @Path("verProduccionAcademicaAlmacenada")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verProduccionAcademicaAlmacenada() {
        return new ProduccionAcademicaModelo().verProduccionAcademicaAlmacenada();
    }
    
    // SERVICIOS PARA LOS INVESTIGADORES
    @POST
    @Path("crearHistorialInvestigador")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearHistorialInvestigador(HistorialInvestigadorGrupo historialInvestigadorGrupo) {
        return new HistorialInvestigadoresModelo().crearHistorialInvestigador(historialInvestigadorGrupo);
    }
  
    @POST
    @Path("crearHistorialConferencias")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta crearHistorialConferencias(HistorialConferenciasInvestigador historialConferenciasInvestigador) {
        return new HistorialInvestigadoresModelo().crearHistorialConferencias(historialConferenciasInvestigador);
    }
    
    @GET
    @Path("verHistorialCompletoInvestigadores")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verHistorialCompletoInvestigadores() {
        return new HistorialInvestigadoresModelo().verHistorialCompletoInvestigadores();
    }
    
    @GET
    @Path("verMiembrosGrupo")
    @Produces(MediaType.APPLICATION_JSON)
    public Respuesta verMiembrosGrupo() {
        return new HistorialInvestigadoresModelo().verMiembrosGrupo();
    }
}
