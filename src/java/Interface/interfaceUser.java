package Interface;

import DTO.Respuesta;
import DTO.GrupoInvestigacion;
import DTO.InsertarCRUD;

public interface interfaceUser {

    public abstract Respuesta insertarCRUDGrupos(InsertarCRUD insertarCRUD);
    public abstract Respuesta insertarCRUDSemilleros(InsertarCRUD insertarCRUD);
}