package Modelo;

import DAO.DatosGlobalesDAO;
import DTO.Respuesta;

public class DatosGlobalesModelo {

    public Respuesta obtenerDatosGlobalesModelo() {
        Respuesta rta = new Respuesta();
        DatosGlobalesDAO datosGlobales = new DatosGlobalesDAO();

        rta = datosGlobales.obtenerDatosCiudadesDAO(rta);
        rta = datosGlobales.obtenerDatosInstitucionesDAO(rta);
        rta = datosGlobales.obtenerDatosAreaConocimientoDAO(rta);
        rta = datosGlobales.obtenerDatosLineaInvestigacionDAO(rta);
        rta = datosGlobales.obtenerDatosTipoProgramaDAO(rta);
        return rta;
    }
    
    public Respuesta verCursos(){
        return new DatosGlobalesDAO().verCursos();
    }
    
}
