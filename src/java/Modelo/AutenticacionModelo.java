package Modelo;

import DAO.AutenticacionDAO;
import DTO.Investigador;
import DTO.Respuesta;

public class AutenticacionModelo {

    public Respuesta registrarUsuario(Investigador investigador) {
        return new AutenticacionDAO().registrarUsuario(investigador);
    }

    public Respuesta iniciarSesion(Investigador investigador) {
        return new AutenticacionDAO().iniciarSesionDAO(investigador);
    }

}
