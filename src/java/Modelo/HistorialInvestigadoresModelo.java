/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import DAO.HistorialInvestigadoresDAO;
import DTO.HistorialConferenciasInvestigador;
import DTO.HistorialInvestigadorGrupo;
import DTO.Respuesta;

/**
 *
 * @author JUAN
 */
public class HistorialInvestigadoresModelo {

    public Respuesta crearHistorialInvestigador(HistorialInvestigadorGrupo historialInvestigadorGrupo) {
        return new HistorialInvestigadoresDAO().crearHistorialInvestigador(historialInvestigadorGrupo);
    }

    public Respuesta crearHistorialConferencias(HistorialConferenciasInvestigador historialConferenciasInvestigador) {
        return new HistorialInvestigadoresDAO().crearHistorialConferencias(historialConferenciasInvestigador);
    }

    public Respuesta verHistorialCompletoInvestigadores() {
        Respuesta rta = new Respuesta();
        HistorialInvestigadoresDAO historialInvestigadoresDAO = new HistorialInvestigadoresDAO();
        rta = historialInvestigadoresDAO.verHistorialInvestigador(rta);
        if (rta.getCodigo() == 1) {
            rta = historialInvestigadoresDAO.verHistorialConferencias(rta);
        }
        return rta;
    }
    
    public Respuesta verMiembrosGrupo(){
        return new HistorialInvestigadoresDAO().verMiembrosGrupo();
    }
}
