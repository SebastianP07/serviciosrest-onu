package Modelo;

import DAO.GrupoInvestigacionDAO;
import DTO.GrupoInvestigacion;
import DTO.MiembrosGrupo;
import DTO.Respuesta;

public class GrupoInvestigacionModelo {

    public Respuesta crearGrupoInvestigacion(GrupoInvestigacion grupoInvestigacion) {
        Respuesta rta = new Respuesta();
        GrupoInvestigacionDAO grupoInvestigacionDAO = new GrupoInvestigacionDAO();

        rta = grupoInvestigacionDAO.crearGrupoInvestigacion(grupoInvestigacion);

        GrupoInvestigacion grupoInvestigacionRta = (GrupoInvestigacion) rta.getObjetoRespuesta();

        if (rta.getCodigo() == 1) {
            
            System.out.println("ID de la tabla " + grupoInvestigacion.getMiembrosGrupo().getIdGrupoInvestigacion());
            
            rta = grupoInvestigacionDAO.crearGrupoInvestigacionMiembrosGrupo(grupoInvestigacionRta);

            GrupoInvestigacion grupoInvestigacionRta2 = (GrupoInvestigacion) rta.getObjetoRespuesta();
            
            if (rta.getCodigo() == 1) {
                rta = grupoInvestigacionDAO.crearGrupoInvestigacionInstitucionesGrupo(grupoInvestigacionRta2);
            }
        }
        return rta;
    }

    public Respuesta verGrupoInvestigacion(){
        return new GrupoInvestigacionDAO().verGrupoInvestigacion();
    }
}
