package Modelo;

import DAO.ProduccionAcademicaDAO;
import DTO.CursoProgramaAcademico;
import DTO.Diplomado;
import DTO.ProgramaAcademico;
import DTO.Respuesta;

public class ProduccionAcademicaModelo {

    public Respuesta crearProgramaAcademico(ProgramaAcademico programaAcademico) {
        return new ProduccionAcademicaDAO().crearProgramaAcademico(programaAcademico);
    }

    public Respuesta crearDiplomado(Diplomado diplomado) {
        return new ProduccionAcademicaDAO().crearDiplomado(diplomado);
    }

    public Respuesta crearCursoProgramaAcademico(CursoProgramaAcademico cursoProgramaAcademico) {
        return new ProduccionAcademicaDAO().crearCursoProgramaAcademico(cursoProgramaAcademico);
    }

    public Respuesta verProgramasAcademicos() {
        return new ProduccionAcademicaDAO().verProgramasAcademicos();
    }

    public Respuesta verProduccionAcademicaAlmacenada() {
        Respuesta rta = new Respuesta();
        ProduccionAcademicaDAO produccionAcademicaDAO = new ProduccionAcademicaDAO();
        rta = produccionAcademicaDAO.verProgramasAcademicosAlmacenados(rta);
        if (rta.getCodigo() == 1) {
            rta = produccionAcademicaDAO.verDiplomadosAlmacenados(rta);
            if (rta.getCodigo() == 1) {
                rta = produccionAcademicaDAO.verCursosProgramaAcademicoAlmacenados(rta);
            }
        }
        return rta;
    }

}
