package DTO;

import java.util.ArrayList;

public class Respuesta {

    private int codigo;
    private String descripcion;
    private String descripcionError;
    private ArrayList<Ciudad> objetoRespuestaArrayCiudades;
    private ArrayList<AreaConocimiento> objetoRespuestaArrayAreaConocimiento;
    private ArrayList<Institucion> objetoRespuestaArrayInstituciones;
    private Investigador objetoRespuestaInvestigador;
    private Object objetoRespuesta;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

<<<<<<< HEAD
    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

    public ArrayList<Ciudad> getObjetoRespuestaArrayCiudades() {
        return objetoRespuestaArrayCiudades;
    }

    public void setObjetoRespuestaArrayCiudades(ArrayList<Ciudad> objetoRespuestaArrayCiudades) {
        this.objetoRespuestaArrayCiudades = objetoRespuestaArrayCiudades;
    }

    public ArrayList<AreaConocimiento> getObjetoRespuestaArrayAreaConocimiento() {
        return objetoRespuestaArrayAreaConocimiento;
    }

    public void setObjetoRespuestaArrayAreaConocimiento(ArrayList<AreaConocimiento> objetoRespuestaArrayAreaConocimiento) {
        this.objetoRespuestaArrayAreaConocimiento = objetoRespuestaArrayAreaConocimiento;
    }

    public ArrayList<Institucion> getObjetoRespuestaArrayInstituciones() {
        return objetoRespuestaArrayInstituciones;
    }

    public void setObjetoRespuestaArrayInstituciones(ArrayList<Institucion> objetoRespuestaArrayInstituciones) {
        this.objetoRespuestaArrayInstituciones = objetoRespuestaArrayInstituciones;
    }

    public Investigador getObjetoRespuestaInvestigador() {
        return objetoRespuestaInvestigador;
    }

    public void setObjetoRespuestaInvestigador(Investigador objetoRespuestaInvestigador) {
        this.objetoRespuestaInvestigador = objetoRespuestaInvestigador;
    }

    public Object getObjetoRespuesta() {
        return objetoRespuesta;
    }

    public void setObjetoRespuesta(Object objetoRespuesta) {
        this.objetoRespuesta = objetoRespuesta;
    }


}
=======
    public ArrayList<Object> getObjetoRespuesta() {
        return objetoRespuesta;
    }

    public void setObjetoRespuesta(ArrayList<Object> objetoRespuesta) {
        this.objetoRespuesta = objetoRespuesta;
    }
}
>>>>>>> 931425e9a4e840eaf93c1720df15d9f003dd6b8d
